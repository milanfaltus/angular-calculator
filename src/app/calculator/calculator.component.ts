import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
})
export class CalculatorComponent implements OnInit {
  leftNumber: number;
  rightNumber: number;
  result: number | string;

  constructor() {
    // FIXME typescript error workaround, therefore, there is same code twice
    this.leftNumber = 0;
    this.rightNumber = 0;
    this.result = 0;
  }

  ngOnInit(): void {
    this.reset();
  }

  add(): void {
    this.result = this.leftNumber + this.rightNumber;
  }

  sub(): void {
    this.result = this.leftNumber - this.rightNumber;
  }

  multiply(): void {
    this.result = this.leftNumber * this.rightNumber;
  }

  divide(): void {
    this.result =
      this.rightNumber === 0
        ? 'Division by zero is undefined.'
        : this.leftNumber / this.rightNumber;
  }

  reset(): void {
    this.leftNumber = 0;
    this.rightNumber = 0;
    this.result = 0;
  }
}
